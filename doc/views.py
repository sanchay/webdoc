from django.contrib.auth.decorators import login_required
from django.views import generic
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import Symptoms, Disease, Blog, Patient, Relation, TypeofDoc
from django.contrib.auth import authenticate, login
from .forms import PSignUpForm, DSignUpForm, CheckerForm


@login_required
def dashboard(request):
    return render(request, 'doc/dashboard.html')


def register(request):
    return render(request, 'registration/register.html')


def p_signup(request):
    if request.method == 'POST':
        p_form = PSignUpForm(request.POST)
        if p_form.is_valid():
            user = p_form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.p_profile.p_dob = p_form.cleaned_data.get('p_dob')
            user.p_profile.p_name = p_form.cleaned_data.get('p_name')
            user.p_profile.p_gender = p_form.cleaned_data.get('p_gender')
            user.p_profile.p_address = p_form.cleaned_data.get('p_address')
            user.p_profile.p_blood_grp = p_form.cleaned_data.get('p_blood_grp')
            user.p_profile.p_contact = p_form.cleaned_data.get('p_contact')
            user.save()
            raw_password = p_form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('/dashboard/')
    else:
        p_form = PSignUpForm()
    return render(request, 'registration/patient_register.html', {'p_form': p_form})


def d_signup(request):
    if request.method == 'POST':
        form = DSignUpForm(request.POST)
        if form.is_valid():
            d_user = form.save()
            d_user.refresh_from_db()  # load the profile instance created by the signal
            d_user.d_profile.d_dob = form.cleaned_data.get('d_dob')
            d_user.d_profile.d_name = form.cleaned_data.get('d_name')
            d_user.d_profile.d_gender = form.cleaned_data.get('d_gender')
            d_user.d_profile.clinic_add = form.cleaned_data.get('clinic_add')
            d_user.d_profile.visit_hours = form.cleaned_data.get('visit_hours')
            d_user.d_profile.d_contact = form.cleaned_data.get('d_contact')
            d_user.d_profile.d_detail = form.cleaned_data.get('d_detail')
            d_user.d_profile.specialization = form.cleaned_data.get('specialization')
            d_user.save()
            raw_password = form.cleaned_data.get('password1')
            d_user = authenticate(username=d_user.username, password=raw_password)
            login(request, d_user)
            return redirect('/dashboard/')
    else:
        form = DSignUpForm()
    return render(request, 'registration/doc_register.html', {'form': form})

"""
def checker(request, ):
    result = Relation.objects.get(sid=)
"""

def index(request):
    return render(request, 'doc/index.html')


def about(request):
    return render(request, 'doc/about.html')


def checker(request):

    if request.method == "POST":
        form = CheckerForm(request.POST)
        if form.is_valid:
            s1 = request.POST.get('Symptom1')
            s2 = request.POST.get('Symptom2')
            s3 = request.POST.get('Symptom3')
            diseases1 = Relation.objects.filter(sid=s1)
            diseases2 = Relation.objects.filter(sid=s2)
            diseases3 = Relation.objects.filter(sid=s3)
            diseases12 = diseases1.filter(
                did__in=diseases2.values_list('did', flat=True)
            )
            diseases = diseases12.filter(
                did__in=diseases3.values_list('did', flat=True)
            )
            return render(request, 'doc/checker.html', {
                'form': form,
                'diseases': diseases,
            })
        else:
            return HttpResponseRedirect('/')
    else:
        form = CheckerForm()
        return render(request, 'doc/checker.html', {
            'form': form,

        })
    return render(request, 'doc/checker.html', diseases)



'''def checker(request):

    return render(request, 'doc/checker.html', {
        'hello':'hello world',
        'form': CheckerForm()

    })'''


class BlogView(generic.ListView):
    template_name = 'doc/blog_list.html'
    context_object_name = 'all_blogs'

    def get_queryset(self):
        return Blog.objects.all()


class SymptomView(generic.ListView):
    template_name = 'doc/symptoms.html'
    context_object_name = 'all_symptoms'

    def get_queryset(self):
        return Symptoms.objects.all()


class PatientsView(generic.ListView):
    template_name = 'doc/patients.html'
    context_object_name = 'all_patients'

    def get_queryset(self):
        return Patient.objects.exclude(p_name__isnull=True).exclude(p_name__exact='')


class PatientDetailView(generic.DetailView):
    model = Patient
    template_name = 'doc/patient_detail.html'


class DiseaseView(generic.ListView):
    template_name = 'doc/diseases.html'
    context_object_name = 'all_diseases'

    def get_queryset(self):
        return Disease.objects.all()


class DisDetailView(generic.DetailView):
    model = Disease
    template_name = 'doc/dis_detail.html'


class BlogDetailView(generic.DetailView):
    model = Blog
    template_name = 'doc/blogpost.html'


class CheckerView(generic.ListView):
    template_name = 'doc/checker.html'
    context_object_name = 'all_symptoms'

    def get_queryset(self):
        return Symptoms.objects.all()


class TodView(generic.ListView):
    template_name = 'doc/tod.html'
    context_object_name = 'all_tod'

    def get_queryset(self):
        return TypeofDoc.objects.all()


class TodDetailView(generic.DetailView):
    model = TypeofDoc
    template_name = 'doc/tod_detail.html'






