from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views

app_name = 'doc'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^checker/$', views.checker, name='checker'),
    url(r'^login', auth_views.login, name='login'),
    url(r'^register/$', views.register, name='register'),
    url(r'^accounts/login/', auth_views.login, name='acc_login'),
    url(r'^logout', auth_views.logout, name='logout'),
    url(r'^patient_register/$', views.p_signup, name='p_signup'),
    url(r'^doctor_register/$', views.d_signup, name='d_signup'),
    url(r'^blog/$', views.BlogView.as_view(), name='blog'),
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    # url(r'^profile/$', views.update_profile, name='profile'),
    url(r'^symptoms/$', views.SymptomView.as_view(), name='symptoms'),
    url(r'^patients/$', views.PatientsView.as_view(), name='patients'),
    url(r'^patients/(?P<pk>[0-9]+)/$', views.PatientDetailView.as_view(), name='patient_detail'),
    url(r'^diseases/$', views.DiseaseView.as_view(), name='diseases'),
    url(r'^diseases/(?P<pk>[0-9]+)/$', views.DisDetailView.as_view(), name='dis_detail'),
    url(r'^blog/(?P<pk>[0-9]+)/$', views.BlogDetailView.as_view(), name='blogpost'),
    url(r'^typeofdoc/(?P<pk>[0-9]+)/$', views.TodDetailView.as_view(), name='tod_detail'),
    url(r'^typeofdoc/$', views.TodView.as_view(), name='tod'),

]
