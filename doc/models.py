
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.
class TypeofDoc(models.Model):
    category = models.CharField(max_length=50)
    detail = models.CharField(max_length=1200)

    def __str__(self):
        return self.category


class Symptoms(models.Model):
    s_name = models.CharField(max_length=100)

    def __str__(self):
        return self.s_name


class Disease(models.Model):
    dis_name = models.CharField(max_length=30)
    dis_type = models.CharField(max_length=20, default="Null")
    dis_detail = models.CharField(max_length=2000)
    medicine = models.CharField(max_length=300, default="Not Available")

    def __str__(self):
        return self.dis_name


class Patient(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='p_profile')
    p_name = models.CharField(max_length=30)
    p_gender = models.CharField(max_length=20)
    p_address = models.CharField(max_length=200)
    p_email = models.CharField(max_length=50)
    p_dob = models.DateField(null=True)
    p_blood_grp = models.CharField(max_length=10)
    p_contact = models.CharField(max_length=20)

    def __str__(self):
        return self.p_name


class Doctor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='d_profile')
    d_name = models.CharField(max_length=30)
    d_gender = models.CharField(max_length=20)
    d_email = models.CharField(max_length=50)
    clinic_add = models.CharField(max_length=200)
    d_dob = models.DateField(null=True)
    visit_hours = models.CharField(max_length=20)
    d_detail = models.CharField(max_length=1200)
    specialization = models.CharField(max_length=30)
    d_contact = models.CharField(max_length=20)

    def __str__(self):
        return self.d_name

class Blog(models.Model):
    did = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=10000)
    time = models.DateTimeField()

    def __str__(self):
        return self.title


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Doctor.objects.create(user=instance)
        Patient.objects.create(user=instance)
    instance.d_profile.save()
    instance.p_profile.save()


'''    class Meta:
        verbose_name = 'p_profile'
        verbose_name_plural = 'profiles'
'''


class PatientRecord(models.Model):
    pid = models.ForeignKey(Patient, on_delete=models.CASCADE)
    mother_name = models.CharField(max_length=30)
    father_name = models.CharField(max_length=30)
    medical_history = models.CharField(max_length=2000)
    physically_challenged = models.BooleanField(default=False)
    mentally_challenged = models.BooleanField(default=False)


class Relation(models.Model):
    sid = models.ForeignKey(Symptoms, on_delete=models.CASCADE)
    did = models.ForeignKey(Disease, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.did)
