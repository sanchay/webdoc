from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Disease, Symptoms, Blog, TypeofDoc, Doctor, Patient, Relation

# Register your models here.


class PatientInline(admin.StackedInline):
    model = Patient
    can_delete = False
    verbose_name_plural = 'Patient'

# Define a new User admin


class DoctorInline(admin.StackedInline):
    model = Doctor
    can_delete = False
    verbose_name_plural = 'Patient'


class UserAdmin(UserAdmin):
    inlines = (PatientInline, DoctorInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)


admin.site.register(Disease)
admin.site.register(Relation)
admin.site.register(Symptoms)
admin.site.register(Blog)
admin.site.register(TypeofDoc)
admin.site.register(Doctor)
admin.site.register(Patient)
