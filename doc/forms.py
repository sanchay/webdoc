from django import forms
from django.forms import extras
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Symptoms


class CheckerForm(forms.Form):
    Symptom1 = forms.ModelChoiceField(queryset=Symptoms.objects.all().order_by('s_name'))
    Symptom2 = forms.ModelChoiceField(queryset=Symptoms.objects.all().order_by('s_name'))
    Symptom3 = forms.ModelChoiceField(queryset=Symptoms.objects.all().order_by('s_name'))


DOY = ('1970', '1971', '1972', '1973', '1974', '1975', '1976', '1977',
       '1978', '1979', '1980', '1981', '1982', '1983', '1984', '1985', '1986', '1987',
       '1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995',
       '1996', '1997', '1998', '1999', '2000', '2001', '2002', '2003',
       '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011',
       '2012', '2013', '2014', '2015', '2016')


class PSignUpForm(UserCreationForm):
    p_name = forms.CharField()
    p_gender = forms.CharField()
    p_address = forms.CharField()
    p_email = forms.EmailField()
    p_dob = forms.DateField(widget=extras.SelectDateWidget(years=DOY,
        empty_label=("Choose Year", "Choose Month", "Choose Day"),),)
    p_blood_grp = forms.CharField()
    p_contact = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'p_name', 'p_gender', 'p_address', 'p_email', 'p_dob', 'p_blood_grp', 'p_contact', 'password1', 'password2', )


class DSignUpForm(UserCreationForm):
    d_name = forms.CharField()
    d_gender = forms.CharField()
    clinic_add = forms.CharField()
    d_email = forms.EmailField()
    d_dob = forms.DateField(widget=extras.SelectDateWidget(years=DOY, empty_label="Nothing"))
    visit_hours = forms.CharField()
    d_detail = forms.CharField()
    specialization = forms.CharField()
    d_contact = forms.CharField()

    class Meta:
        model = User
        fields = ('username', 'd_name', 'd_gender', 'clinic_add', 'd_email', 'd_dob', 'visit_hours', 'd_detail', 'specialization', 'd_contact', 'password1', 'password2', )
